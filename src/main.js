import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import firebase from "firebase";

Vue.config.productionTip = false;
var firebaseConfig = {
  apiKey: "AIzaSyAmjdA0rRM0PZ87Hy6BapNnIOgRrUWxKp8",
  authDomain: "loginsocial-18da1.firebaseapp.com",
  projectId: "loginsocial-18da1",
  storageBucket: "loginsocial-18da1.appspot.com",
  messagingSenderId: "616835560183",
  appId: "1:616835560183:web:a31af8f7e53939024a59f2",
  measurementId: "G-V0MZP8G8WJ"
};
firebase.initializeApp(firebaseConfig);
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
