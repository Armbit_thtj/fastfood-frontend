import Vue from "vue"
import VueRouter from "vue-router"
import Home from "../views/Home.vue"
import Dashboard from "../views/Dashboard.vue"
import About from "../views/About.vue"
import Main from "../views/Main"
import Login from "../views/Login.vue"
import Employee from "../views/Employee.vue"
import Profile from "../views/Profile.vue"
import Booking from "../views/Booking.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "Main",
    component: Main,
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: Dashboard
      },
      {
        path: "employees",
        name: "Employee",
        component: Employee
      },
      {
        path: "profile/:employee_id",
        name: "Profile",
        component: Profile
      },
      {
        path: "booking",
        name: "Booking",
        component: Booking
      }
    ]
  },
  {
    path: "/login",
    name: "Login",
    component: Login
  }
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
})

export default router
