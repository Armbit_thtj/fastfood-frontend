const jwt = require("jsonwebtoken")

const decodeAccessToken = async (token) => {
  const decoded = await jwt.decode(token)
  return decoded
}

export { decodeAccessToken }
