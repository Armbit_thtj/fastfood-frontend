import axios from "axios"

const get = async (url) => {
  const { data, message } = await axios.get(url)
  return data
}

const axiosGet = async (url, token) => {
  try {
    const axiosInstance = axios.create({
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    })

    axiosInstance.interceptors.response.use(
      (response) => response,
      (error) => {
        if (error.response.status === 401) {
          // Unauthorized()
          return error
        }
        return error
      }
    )

    const { data, status } = await axiosInstance.get(url)
    return data
  } catch (error) {
    return error
  }
}

const post = async (url, body, content_type) => {
  const { data, message } = await axios.post(url, body, {
    headers: {
      "Content-Type": content_type
    }
  })
  return data
}

const postFile = async (url, body) => {
  const { data, message } = await axios.post(url, body, {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  })
  return data
}
const put = async (url, body, content_type) => {
  const { data, message } = await axios.put(url, body, {
    "Content-Type": content_type
  })
  return data
}
const destroy = async (url, params) => {
  const { data, message } = await axios.delete(url)
  return data
}

export { get, axiosGet, post, postFile, put, destroy }
