const API_URL = "api/v1"
const DOMAIN = "http://localhost:3300"
const FOOD_STATIC_PATH = "http://localhost:3300/foods/images/"
const EMPLOYEE_STATIC_PATH = "http://localhost:3300/employees/images/"

export { API_URL, DOMAIN, FOOD_STATIC_PATH, EMPLOYEE_STATIC_PATH }
